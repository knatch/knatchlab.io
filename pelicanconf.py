#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

SITENAME = 'Natch Khongpasuk\'s Project'
SITEDESCRIPTION = 'Natch Khongpasuk is a Software Engineer, DevOps Enthusiast and Fixie raider. knatch.me contains Natch\'s personal projects, interests, writings and thoughts.'
# SITEURL = ''
AUTHOR = 'Natch Khongpasuk'

PATH = 'content'

STATIC_PATHS = [
    'extra'
]

EXTRA_PATH_METADATA = {
    'extra/apple-touch-icon.png': {'path': 'apple-touch-icon.png'},  # favicon
    'extra/android-chrome-192x192.png': {'path': 'android-chrome-192x192.png'},  # favicon
    'extra/android-chrome-512x512.png': {'path': 'android-chrome-512x512.png'},  # favicon
    'extra/favicon.ico': {'path': 'favicon.ico'},  # favicon
    'extra/favicon-16x16.png': {'path': 'favicon-16x16.png'},  # favicon
    'extra/favicon-32x32.png': {'path': 'favicon-32x32.png'},  # favicon
    'extra/site.webmanifest': {'path': 'site.webmanifest'},  # favicon
    'extra/fonts/Metropolis/': {'path': 'fonts/Metropolis/'}
}

DIRECT_TEMPLATES = ['blog', 'projects']
PAGINATED_DIRECT_TEMPLATES = ['blog', 'projects']

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
         # ('Python.org', 'http://python.org/'),
         # ('Jinja2', 'http://jinja.pocoo.org/'),
         # ('You can modify those links in your config file', '#'),)

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
          # ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

THEME = './theme'

PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'

# disable caching
LOAD_CONTENT_CACHE = False

# GOOGLE_ANALYTICS = '117936399'