title: Reference
save_as: reference/index.html
description: Reference materials I've gathered over the years of surfing the vast space of world wide web

Interesting reads, blogs and sites I've gathered over the years of surfing the vast space of the World Wide Web

### Privacy-focused
- <a href="https://www.privacytools.io" target="_blank" rel="noopener">privacy tools</a> and <a href="https://www.privacyguides.org" target="_blank" rel="noopener">privacy guides</a>
- <a href="https://fuckoffgoogle.de" target="_blank" rel="noopener">f off, google</a>
- <a href="http://www.socialcooling.com" target="_blank" rel="noopener">social cooling</a>

### Software engineers' blogs
- <a href="https://blog.codinghorror.com/" target="_blank" rel="noopener">Coding horror</a>
- <a href="https://phse.net/" target="_blank" rel="noopener">Stephen Lindberg</a>
- <a href="https://scattered-thoughts.net/" target="_blank" rel="noopener">Jamie Brandon</a>
- <a href="https://operand.ca/" target="_blank" rel="noopener">James MacMahon</a>
- <a href="https://sethrobertson.github.io/" target="_blank" rel="noopener">Seth Robertson</a>
- <a href="https://www.baldurbjarnason.com" target="_blank" rel="noopener">Baldur Bjarnason</a>
    - <a href="https://www.baldurbjarnason.com/2021/100-things-every-web-developer-should-know/" target="_blank" rel="noopener">136 facts every web dev should know before they burn out</a>
- <a href="https://danluu.com">Dan Luu</a>
    - <a href="https://danluu.com/look-stupid/" target="_blank" rel="noopener">Look stupid</a>

### Misc.
- <a href="https://tosdr.org" target="_blank" rel="noopener">Terms of Service, didn't read</a>
- <a href="http://wayback.archive.org" target="_blank" rel="noopener">waybach machine</a>