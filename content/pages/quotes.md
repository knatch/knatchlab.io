title: Quotes
save_as: quotes/index.html
description: A random collection of inspirational quotes I have gathered over the years from all over the place.

This is a random collection of inspirational quotes which I have gathered from over the place. Often, I would find myself mentally recite the following citations to lift up my spirit. 

While this is a sample of my interests, it is not necessarily a completely representative sample.

<blockquote>
    <p>"We are what we repeatedly do. Excellence then, is not an act, but a habit."</p>
    <footer>– Aristotle</footer>
</blockquote>

<blockquote>
    <p>"Embarrassment is the cost of entry."</p>
</blockquote>

<blockquote>
    <p>"The problem is not the problem. The problem is your attitude about the problem."</p>
    <footer>– Captain Jack Sparrow</footer>
</blockquote>

<blockquote>
    <p>"Worrying means you suffer twice."</p>
    <footer>– Newton Scamander</footer>
</blockquote>

<blockquote>
    <p>"Worry is a misuse of imagination."</p>
</blockquote>

<blockquote>
    <p>"Why would you think about missing a shot You haven’t taken yet."</p>
    <footer>– Michael Jordan</footer>
</blockquote>

<blockquote>
    <p>"No discomfort no expansion"</p>
    <footer>– Nathan W. Pyle</footer>
</blockquote>