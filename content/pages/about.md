title: About Me
save_as: about/index.html
description: Sawasdee (hello)! The name is Natch and this is my about page...
category: About
status: hidden


This is my about page. It is supposed to be a story of myself, my semi-personal detail and interests.


Sawasdee (hello)! The name is Natch. I am a graduate from Chiang Mai University with a B.E. in Computer Engineering (2015). In my early years, I lived in Yala, the southernmost province of Thailand. I moved to Chiang mai during the start of [insurgency](https://en.wikipedia.org/wiki/South_Thailand_insurgency){:target="_blank" rel="noopener"} in the area.

Fast forward a few years, I attended a Bachelor of science in computer engineering program at [Chiang Mai University](https://cmu.ac.th/){:target="_blank" rel="noopener"}. During this period, I got exposed to multiple facets and possibilities of computer science, ie. networking, embeded system, robots, artificial intelligence, software development, game development, up-and-coming data science and the list goes on. Do not take this the wrong way, the program does a good job covering basic knowledge on every topic in the list. However, being an indesicive late teenager I was, I could not settle my mind on what and where I should begin my career.

To be continued..
