title: Now
save_as: now/index.html
description: My now page – simply a list of things I am currently focusing on in my life...
slug: now

This is my now page. The purpose of this page is to help me put more thought into how I should be spending my <s>valuable</s> limited time.

## General

This is the state of things as of October 2021.

- Working as a frontend developer at <a href="https://mindera.com" target="_blank" rel="noopener">Mindera</a> for <a href="https://amwell.com/cm/" target="_blank" rel="noopener">Amwell</a>
- Building an app with Simon in my free time

<!--  

This is the state of things as of September 2021.

- Working as a frontend developer at <a href="https://mindera.com" target="_blank" rel="noopener">Mindera</a> for <a href="https://amwell.com/cm/" target="_blank" rel="noopener">Amwell</a>

This is the state of things as of April 2021.

- Actively seeking <a href="https://www.linkedin.com/in/natch-khongpasuk" target="_blank" rel="noopener" title="Hire me"><strong>a full-time opportunity</strong></a>
- Building a platform for a <a href="http://cirtausa.com/" target="_blank" rel="noopener">startup</a>


This is the state of things as of February 2021.

- Parcipating at <a href="https://ortelius.io/2021/01/20/ortelius-blog-a-thon/" target="_blank" rel="noopener">Ortelius BlogAThon</a>
- Actively seeking <a href="https://www.linkedin.com/in/natch-khongpasuk" target="_blank" rel="noopener" title="Hire me"><strong>a full-time opportunity</strong></a>
- Building a static <a href="https://github.com/shanonmcdonald/CirtaTIC-site/" target="_blank" rel="noopener">site</a> for local business

## Goals

- Relearn React with Next.js
- Finish <a href="https://interpreterbook.com/" target="_blank" rel="noopener">Golang the interpreter book</a>

This is the state of things as of January 2021.

- Graduate a Master's Degree in Cybersecurity and Privacy from <a href="https://www.njit.edu/" target="_blank" rel="noopener">NJIT</a>
- Actively seeking <a href="https://www.linkedin.com/in/natch-khongpasuk" target="_blank" rel="noopener" title="Hire me"><strong>a full-time opportunity</strong></a>
- Commit to contribute to open source project <a href="https://ortelius.io/" target="_blank" rel="noopener">Ortelius</a>

This is the state of things as of November 2020.

- Actively seeking <a href="https://www.linkedin.com/in/natch-khongpasuk-4026b8b9/" target="_blank" rel="noopener" title="Hire me"><strong>a full-time opportunity</strong></a> still
- Residing in the United States and pursuing a Master's Degree in Cybersecurity and Privacy at <a href="https://www.njit.edu/" target="_blank" rel="noopener">NJIT</a>
- Have been working as a Developer for <a href="http://telus-national.org/" target="_blank" rel="noopener">TELUS</a> since Febuary 2019

## Goals
- Will be taking <a href="https://aws.amazon.com/certification/certified-solutions-architect-associate/" target="_blank" rel="noopener">Amazon Certified Solutions Architect - Associate</a> exam in the second week of the month

This is the state of things as of October 2020.

## General

- Actively seeking <a href="https://www.linkedin.com/in/natch-khongpasuk-4026b8b9/" target="_blank" rel="noopener" title="Hire me"><strong>a full-time opportunity</strong></a>
- Residing in the United States pursuing a Master's Degree in Cybersecurity and Privacy at <a href="https://www.njit.edu/" target="_blank" rel="noopener">NJIT</a>
- Have been working as a Developer for <a href="http://telus-national.org/" target="_blank" rel="noopener">TELUS</a> since Febuary 2019

## Goals
- Preparing for <a href="https://aws.amazon.com/certification/certified-solutions-architect-associate/" target="_blank" rel="noopener">Amazon Certified Solutions Architect - Associate</a> -->
