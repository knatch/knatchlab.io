Title: My AWS Certified Security - Specialty Experience
Slug: my-aws-certified-security-specialty-experience
Date: 2022-09-01
Category: blog
Tags: certifications
Template: article
Description: I cleared AWS Certified Security - Specialty certification on my first try somehow and I am proud of myself. Here's the materials I studied...

This is my shiny [badge](https://www.credly.com/badges/2e371545-282a-4a5d-8264-48ee18442149/).

I cleared AWS Certified Security - Specialty certification on my first try. It wasn't easy. I flagged at least 6 questions of the first 10. When I got to question 30, I was fairly certain I have to retake the exam soon. I had 5 minutes left on my timer when I answered the last question.

I passed it, somehow.

What makes the exam challenging for me is that I don't get to perform a security role in my day to day software developer work (but don't we all have responsibilities to be aware of the security matter, in some way). My company does not require me to obtain this certification. I take the exam because I want to validate my expertise in AWS cloud security. Perhaps this certification could get me into a more security-focused role. I hope it would.

After I decided to take the exam, the first thing I did was going over the official [Exam Guide](https://d1.awsstatic.com/training-and-certification/docs-security-spec/AWS-Certified-Security-Specialty_Exam-Guide.pdf). The document outlines the details of the exam; weightings, test domains, objectives, exam duration, etc. This should give you an idea of how you want to prepare for the exam.

The next thing I find very helpful is this blog post: [10 tips to study](https://aws.amazon.com/blogs/training-and-certification/10-tips-to-study-for-the-aws-certified-security-specialty-certification/). **Be paranoid** popped in my head every time I wasn't too sure between 2 choices. Although, as it is also mentioned in the post, there are questions that ask for "the most cost friendly solution" or "the least admistrative overhead way". These are times where you have to make sure you understand what the question actually wants.

Before diving into the hands-on labs or exercises, take some time to study the recommended AWS whitepapers posted on the AWS certified security specialty certification page. These whitepapers provide the overview of AWS services as well as best practices to learn from the experts at AWS. I spent a good amount of time on these 3 papers:

- [AWS Security Best Practices](https://d1.awsstatic.com/whitepapers/aws-security-best-practices.pdf)
- [AWS Security Incident Response Guide](Security at Scale: Logging in AWS)
- [Security at Scale: Logging in AWS](https://d1.awsstatic.com/whitepapers/compliance/AWS_Security_at_Scale_Logging_in_AWS_Whitepaper.pdf)

![Book](https://static.packt-cdn.com/products/9781789534474/cover/smaller)

I have this [AWS Certified Security - Specialty Exam Guide, Packt](https://www.packtpub.com/product/aws-certified-security-specialty-exam-guide/9781789534474) sitting in my Humble Bundle library for years. It waited for me to pick it up and study it for the exam. I'm glad I did. The book covers all the domains, both in theory and practice. It also provides mock tests in the last chapter for you to review.

From the excerpt of the book:

> By the end of this AWS security book, you'll have the skills to pass the exam and design secure AWS solutions.

The night before.

That's a lot of reading. Wouldn't it be nice to listen to someone? Bonus point, that certain someone is an expert in the area you're going to take the exam. I did what I did when I was going to take the AWS Certified Developer Associate exam. I reviewed the [Exam Readiness](https://explore.skillbuilder.aws/learn/course/external/view/elearning/97/exam-readiness-aws-certified-security-specialty?ss=sec&sec=prep) course. It explores exam topic areas and reviews sample exam questions in each area. It really freshened my memory on all topics on the night before the exam.

That's it. I thought I'd prepared well but the test still managed to almost throw me off. Perhaps I, in fact, did not prepare well. Or perhaps I took the first exam tip; *be paranoid* too seriously. Despite feeling like giving up several times, I'm happy did not. I studied, I did some work and the result is really worth the effort.

<iframe src="https://giphy.com/embed/xT5LMHxhOfscxPfIfm" width="480" height="362" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/season-17-the-simpsons-17x10-xT5LMHxhOfscxPfIfm"></a></p>

__Note__ These hands-on labs on [Well Architected Labs](https://www.wellarchitectedlabs.com/security/) will bring you some "gotcha" moment after the long study.

