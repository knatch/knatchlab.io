Title: Capstone Project Architectural Choices
Slug: capstone-project-architectural-choices
Date: 2020-11-12
Category: blog
Tags: writing,software development,software engineering
Template: article
Description: Instead of developing one gigantic program in monolithic style that will contain all of the required functionalities, the industry team decides that the project should be developed in the microservices architecture.

<small>This is part of the Fall 2020 Capstone Project YWCC 691. The class participation assignment asks to write about the architectural decisions or choices that I made during the course of my capstone project. Do highlight where my choice may have been discarded.</small>

Instead of developing one gigantic program in monolithic style that will contain all of the required functionalities, the industry team decides that the project should be developed in the microservices architecture. This allows each component to have a specific functionality with a specific set of input and output. The components are designed to run and work separately in its own container.

Since the beginning, the industry team has outlined the desired architectural framework as well as the production environment that the system will be deployed to. The team chooses Google Cloud Platform as a cloud production infrastructure and employs Apache Airflow, a framework for managing the passing data from component to component, for the students. The student team’s responsibility is to develop the components that will be orchestrated by the Apache Airflow framework. In addition, the students are also responsible for the configuration of Apache Airflow to properly run the components.

Although, initially it seems like the architectural decision is set but the student team still gets to decide what and how each of the components communicate. I was assigned a role to oversee the Apache Airflow configuration along with connecting the components to properly pass the output data. In the first sprint, we all had a different vision in our heads. Four components were developed and everyone claimed that it worked well on their end. However, when I had to deploy them on Apache Airflow, none of the components worked.

<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/bI5BEfwbdVPcA" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div>

This is when I learned that Google Cloud Platform offers 2 services that a component could be deployed on in order to trigger it from Apache Airflow webserver. Those services are Cloud Run and Cloud Function. I research the pros and cons of the services, ie. how Apache Airflow will trigger them, what intermediate storage will be, how each component can provide an API to communicate with one another, etc. I later present the solutions to the industry team and eventually one gets approved. This results in some trivia code change. After wrapper functions are added to the components, we are able to operate our first-phase components in the production Apache Airflow environment.

To conclude, despite the fact that the students will only develop the system in a very short period, I believe being able to come up with an architectural decision and actually get accepted gives a tremendous sense of software ownership as well as accountability to the student team. Not only did this decision solve our configuration issue, it also paved the way for the upcoming components in the next phase.